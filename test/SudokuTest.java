
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;


public class SudokuTest {
    private Sudoku sudoku;
    
    @Before
    public final void setup() {
        sudoku = new Sudoku("./Root/res/board.txt");
    }
    
    /**
     * QUESTION 1:
     * 
     * This test should load the board from the file
     * (in the @Before) and ensure that the values
     * were loaded correctly (including a couple of
     * representative 0 values).
     */
    @Test
    public final void testLoadBoardFromFile() {
        assertEquals(4, sudoku.getNumber(0, 1));
        assertEquals(1, sudoku.getNumber(0, 3));
        assertEquals(3, sudoku.getNumber(1, 0));
        assertEquals(4, sudoku.getNumber(2, 3));
        assertEquals(0, sudoku.getNumber(2, 1));
        assertEquals(0, sudoku.getNumber(0, 2));
    }

    /**
     * QUESTION 2:
     * 
     * This test should ensure that the bottom-right
     * subsquare is sliced correctly. It should be
     *    04
     *    00
     */
    @Test
    public final void testGetBottomRightSubsquare() {
        int[][] expected = {{0, 4}, {0, 0}};
        int[][] subsquare = sudoku.getSubsquare(1, 1);
        assertArrayEquals(expected, subsquare);
    }
    
    /**
     * QUESTION 3:
     * 
     * This test should ensure that the sum of the 
     * second row (index 1) is equal to 3.
     */
    @Test
    public final void testGetSumOfSecondRow() {
        assertEquals(3, sudoku.getRowSum(1));
    }
    
    /**
     * QUESTION 4:
     * 
     * This test should ensure that the sum of the 
     * fourth column (index 3) is equal to 5.
     */
    @Test
    public final void testGetSumOfFourthColumn() {
        assertTrue(false);
    }
    
    /**
     * QUESTION 5:
     * 
     * This test should ensure three cases:
     *   + setting the value 2 in (1, 3) is legal
     *   + setting the value 4 in (1, 3) is illegal
     *   + setting the value 2 in (2, 3) is illegal
     */
    @Test
    public final void testIsLegalMove() {
        assertTrue(false);
    }
}

